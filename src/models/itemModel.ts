import { Recipe } from './recipeModel';
export class Item {
    id : number;
    name : string;
    recipes : Array<Recipe>;
    cuisine : string;
    created_at : string;
    updated_at : string;
}