export class Ingredient {
    id : number;
    content : string;
    recipe_id : number;
    created_at : string;
    updated_at : string;
    image_url : string;
}