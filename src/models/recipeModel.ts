import { Instruction } from './instructionModel';
import { Ingredient } from './ingredientModel';

export class Recipe {
    id : number;
    item_id : string;
    name : string;
    chef_name : string;
    cook_time : string; 
    instructions : Array<Instruction>;
    ingredients : Array<Ingredient>;
    created_at : string;
    updated_at : string;
    image_url : string;
    about_info : string;
}