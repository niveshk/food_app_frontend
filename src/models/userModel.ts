export class User {
  id : number;
  provider : string;
  uid : string;
  name : string;
  created_at : string;
  updated_at : string;
  recipe_id : number;
}