import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service';

@IonicPage()
@Component({
  selector: 'page-nutrition',
  templateUrl: 'nutrition.html',
})
export class NutritionPage {
  sizeOptions : Array<any> = [];
  nutrition: any;
  selectedSize;
  servings : number = 1;
  amountInGrams : number = 200;
  gramsPerServing : number;
  constructor(public navParams: NavParams, public dataService: DataServiceProvider, public navCtrl: NavController) {
    console.log("NavParams: ", this.navParams);
    this.nutrition = this.navParams.data['nutrition'];
     console.log("Nutrition", this.nutrition);
    this.gramsPerServing = this.navParams.data['data']['grams_per_serving'];
    }
  
  
  /*ionViewCanLoad() {
    return new Promise((resolve, reject) => {
      
    }
    );
  }
  */
}
