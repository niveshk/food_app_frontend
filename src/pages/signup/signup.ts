import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ItemsPage } from '../items/items';
import { MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { ValidationServiceProvider } from '../../providers/validation-service/validation-service'
import { LoginPage } from '../login/login'
import { FacebookAuth, User, AuthLoginResult } from '@ionic/cloud-angular';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage implements OnInit {
  signUpForm: FormGroup;
  errors: Array<string>
  fbDetails: AuthLoginResult
  
  url = "http://food-app-thenightsaredarkandfullofterrors.c9users.io/auth/";
  constructor(private fb: FacebookAuth, private user: User,public dataService: DataServiceProvider, public storage: Storage, public menuController: MenuController, public authService: AuthServiceProvider, public navCtrl: NavController, private formBuilder: FormBuilder, public navParams: NavParams, private validationService: ValidationServiceProvider) {
    this.buildSignUpForm()
  }
  async fblogin():Promise<void>{
    console.log('Yay..!')
    this.fbDetails = await this.fb.login();
    console.log('Details', this.fbDetails) 
     this.fb.login().then((data)=>{
       console.log('data -> ', data)
     })
     this.fb.login().then((data)=>{
       console.log('data -> ', data)
     })
   console.log(this.user)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }
  ngOnInit() {

  }
  buildSignUpForm() {
    let validationService = this.validationService
    this.signUpForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, validationService.emailValidator]],
      password: ['', [Validators.required, validationService.passwordValidator]],
      password_confirmation: ['', Validators.required],
      role: "user"
    })
  }
  goToLogin() {
    this.navCtrl.push(LoginPage)
  }
  signUp() {
    if (this.signUpForm.dirty && this.signUpForm.valid) {
      this.authService.signUp(this.signUpForm.value).subscribe(res => {
        if (res.json()["status"] == "success") {

          let token = res.headers.get('access-token');
          let userData = res.json()["data"];
          this.dataService.saveUserData(userData);
          this.dataService.saveToken(token);
          this.enableAuthMenu();
          this.navCtrl.push(ItemsPage);
        }
      }, error => {
        this.errors = error.json().errors
      })

    }

  }
  enableUnauthMenu() {
    this.menuController.enable(true, 'unauthenticated');
    this.menuController.enable(false, 'authenticated');
  }
  enableAuthMenu() {
    this.menuController.enable(false, 'unauthenticated');
    this.menuController.enable(true, 'authenticated');
  }
}
