import { NgModule } from '@angular/core';
import { IonicPageModule, Nav } from 'ionic-angular';
import { RecipePage } from './recipe';

@NgModule({
  declarations: [
    RecipePage,
  ],
  imports: [
    IonicPageModule.forChild(RecipePage),
  ],
  exports: [
    RecipePage
  ],
  providers: [Nav]
  
})
export class RecipePageModule {}
