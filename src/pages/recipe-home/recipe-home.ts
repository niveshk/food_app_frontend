import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Recipe } from '../../models/recipeModel';
import { DataServiceProvider } from '../../providers/data-service/data-service';
@Component({
  selector: 'page-home',
  templateUrl: 'recipe-home.html'
})
export class RecipeHomePage implements OnInit {
  item: any
  recipe: Recipe;
  singleValue = 10;
  reviews : Array<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams, private dataService : DataServiceProvider) {
    
  }
  ngOnInit() {
    this.recipe = this.navParams.get('data')
    this.item = this.navParams.get('item')
    this.getReviews();

  }
 
  getReviews(){
   this.dataService.getReviews(this.recipe).subscribe(reviews => {
      this.reviews = reviews.json();
      console.log("These are the reviews", reviews);
    })
  }
}


