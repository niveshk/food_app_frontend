import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service'
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CartPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage implements OnInit {
  items: Array<any> = [];
  url: string = 'http://food-app-thenightsaredarkandfullofterrors.c9users.io:8080';
  total_price: number = 0;
  user_id: number;
  constructor(private toastCtrl: ToastController, public _http: Http, public storage: Storage, public dataService: DataServiceProvider, public navCtrl: NavController, public navParams: NavParams) {

  }


  ngOnInit() {
    this.storage.get('userData').then(data => {
      this.user_id = JSON.parse(data).id;

      this.dataService.getCartItems(this.user_id).subscribe((data) => {
        this.items = JSON.parse(data)
        this.items.forEach(element => {
          this.total_price += parseInt(element.price)
        });
      })
    })
  }

  removeItem(item) {
    this._http.delete(`${this.url}/users/${this.user_id}/cart_items/${item.id}`)
      .subscribe(
      (res) => {
        if (res.status == 200) {
          let toast = this.toastCtrl.create({
            message: 'Item successfully deleted from your cart',
            duration: 3000,
            position: 'middle'
          })
          toast.present()
          this.items.splice(this.items.indexOf(item), 1)
          this.total_price -= parseInt(item.price)

        }
        else {
          let toast = this.toastCtrl.create({
            message: 'Item could NOT be deleted from your cart',
            duration: 3000,
            position: 'middle'
          })
          toast.present()
        }
      },
      (error) => {
        console.log("error")
        let toast = this.toastCtrl.create({
          message: 'Item could NOT be deleted from your cart',
          duration: 3000,
          position: 'middle'
        })
        toast.present()
      }
      )
  }

}




