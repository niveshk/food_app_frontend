import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PocketPage } from './pocket';

@NgModule({
  declarations: [
    PocketPage,
  ],
  imports: [
    IonicPageModule.forChild(PocketPage),
  ],
  exports: [
    PocketPage
  ]
})
export class PocketPageModule {}
