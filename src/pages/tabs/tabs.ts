import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RecipeHomePage } from '../recipe-home/recipe-home';
import { RecipePage } from '../recipe/recipe';
import { NutritionPage } from '../nutrition/nutrition';
import { ItemServiceProvider } from '../../providers/item-service/item-service';
import { DataServiceProvider } from '../../providers/data-service/data-service';
@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
  providers: [ItemServiceProvider],
  outputs: ['recipe_data']
})
export class TabsPage implements OnInit {
  home;// = RecipeHomePage
  nutritionData : any;
  ing;// = RecipePage
  nutrition; //= NutritionPage
  item_id: any
  recipe_data = {};//= {}
  ingredients: any
  ingStatuses: any
  //recipe: any;
  constructor(public navCtrl: NavController, public dataService : DataServiceProvider,public navParams: NavParams, public itemService: ItemServiceProvider) {
    this.item_id = this.navParams.get('data');
  }
  ngOnInit() {
    //console.log(this.itemService)

  }
  assignData(data) {
    this.itemService.getItemRecipesById(data['id'])
      .subscribe((data1) => {
        this.recipe_data = JSON.parse(data1);
        this.home = RecipeHomePage
        this.ing = RecipePage
        this.nutrition = NutritionPage
      });

  }
  ionViewCanEnter() {
    return new Promise((resolve, reject) => {
      this.itemService.getItemRecipesById(this.item_id)
        .subscribe((data1) => {
          this.recipe_data = JSON.parse(data1);
          this.dataService.getNutrition(this.item_id, this.recipe_data['id']).subscribe(
          (res) => {
            this.nutritionData = res.json();
            console.log(this.nutritionData);
          }
        );
          this.home = RecipeHomePage
          this.ing = RecipePage
          this.nutrition = NutritionPage
          resolve();
        });
    })

  }
}
