import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RatingPage } from '../rating/rating';
import { DataServiceProvider } from '../../providers/data-service/data-service'
import { Instruction } from '../../models/instructionModel';
import { ItemServiceProvider } from '../../providers/item-service/item-service';
@IonicPage()
@Component({
  selector: 'page-recipe-tour',
  templateUrl: 'recipe-tour.html'
})
export class RecipeTourPage {
  reviewFormHidden : boolean = true;
  reviewButtonHidden : boolean = false;
  ready = {
    image: 'http://img.taste.com.au/Io6RE4uz/taste/2016/11/mixed-vegetable-curry-89572-1.jpeg',
  }//final recipe image 
  instructions: Array<Instruction>;
  item_id: number;
  recipe: any;
  instructionStatuses: any = {};
  user_id: number;
  rating: any = 0;;
  review: string;
  review_status: boolean = false;
  
  constructor(public alertCtrl: AlertController, public itemService: ItemServiceProvider, public dataService: DataServiceProvider, public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
    this.recipe = navParams.get('recipe')
    this.item_id = this.recipe.item_id
    this.user_id = navParams.get('user_id');
   // console.log('user_id', this.user_id)
    //console.log('item_id', this.item_id)
    //console.log('recipe', this.recipe)
    this.dataService.getInstructionStatuses(this.recipe.id).then((val) => {
      if (val === null) {
        if (this.user_id != null) {
          this.dataService.getInstructionStatusesFromApi(this.recipe.id, this.user_id).subscribe((res) => {
            this.instructionStatuses = res.json();
            this.assignInstructions(this.item_id, this.recipe.id);
          })
        }
        else this.assignInstructions(this.item_id, this.recipe.id);
      }
      else {
        this.instructionStatuses = JSON.parse(val);
        this.assignInstructions(this.item_id, this.recipe.id);
      }
    }).catch(() => {
      if (this.user_id != null) {
        this.dataService.getInstructionStatusesFromApi(this.recipe.id, this.user_id).subscribe((res) => {
          this.instructionStatuses = res.json();
          this.assignInstructions(this.item_id, this.recipe.id);
        })
      }
      else this.assignInstructions(this.item_id, this.recipe.id);
      this.assignInstructions(this.item_id, this.recipe.id);

    });

    // let ingredients = navParams.get('a')
  }
  assignInstructions(item_id, recipe_id) {
    this.itemService.getInstructions(item_id, recipe_id).subscribe((data) => {
      let parsed_data = JSON.parse(data);
      // console.log(parsed_data)
      this.instructions = parsed_data;
      if (this.instructionStatuses === null) {
        this.instructionStatuses = {}
        for (let i = 0; i < parsed_data.length; i++) {
          this.instructionStatuses[`${parsed_data[i].id}`] = false;
        }

      }
      //console.log(this.instructionStatuses);
    });
  }

  backToItems() {
    this.navCtrl.pop();

  }


  statusChange(event, id) {
    this.instructionStatuses[id] = event.value;
    this.dataService.saveInstructionStatuses(this.instructionStatuses, this.recipe.id)
  }
  
  reviewAlert() {
    let prompt = this.alertCtrl.create({
      title: 'Review',
      message: 'Write a review about this recipe',
          inputs: [{
          name: 'content',
        placeholder: 'I really loved this recipe!!!'
      }],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Rate',
          handler: data => {
            this.pushRating(data)
          }
        }
      ]

    })

    prompt.present()

  }
  pushRating(data){

    this.review_status = true;
    this.navCtrl.push(RatingPage,{review: data.content,recipe_id: this.recipe.id})

  }
  

} 