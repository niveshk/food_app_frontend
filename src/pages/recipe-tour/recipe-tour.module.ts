import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecipeTourPage } from './recipe-tour';

@NgModule({
  declarations: [
    RecipeTourPage,
  ],
  imports: [
    IonicPageModule.forChild(RecipeTourPage),
  ],
  exports: [
    RecipeTourPage
  ]
})
export class RecipeTourPageModule { }
