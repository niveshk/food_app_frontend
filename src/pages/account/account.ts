import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import {MenuController} from 'ionic-angular';
import { ItemsPage } from '../items/items';
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { ItemServiceProvider } from '../../providers/item-service/item-service';


@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage implements OnInit {
  name: string;
  user: any = {};
  dataArray = [];
  items: Array<any>;
  constructor(public itemService: ItemServiceProvider, public dataService : DataServiceProvider,public menuController: MenuController, public http : Http, public storage: Storage, public navCtrl: NavController, public navParams: NavParams, public authService: AuthServiceProvider) {
    this.getUserDetails();
 }
  ngOnInit(){
      
  }
  getUserDetails() {
      this.dataService.getData().then((val) =>{
        this.user = JSON.parse(val);
        console.log(this.user)
    });
   
  }
  logOut(){
    this.getUserDetails();
    this.dataService.postStatusData(this.user.id);
    this.dataService.clearStorageOfAuthDetails();
    this.storage.set('lastUsedEmail', this.user.email);
    this.enableUnauthMenu();
    this.navCtrl.setRoot(ItemsPage);
  }
  enableUnauthMenu(){
    this.menuController.enable(true, 'unauthenticated');
    this.menuController.enable(false, 'authenticated');
  }

}
