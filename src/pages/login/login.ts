import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ItemsPage } from '../items/items';
import { MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { SignupPage } from '../signup/signup';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DataServiceProvider } from '../../providers/data-service/data-service';
import { ValidationServiceProvider } from '../../providers/validation-service/validation-service'
//import { FacebookAuth, User, AuthLoginResult } from '@ionic/cloud-angular';
//import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  userData: any;
  logInForm: FormGroup;
  authToken: any;
  url: string = 'http://food-app-thenightsaredarkandfullofterrors.c9users.io/auth/';
  LastUsedEmail = ''
  errors: Array<string>
  //fbDetails: AuthLoginResult

  constructor( public dataService: DataServiceProvider, public http: Http, public menuController: MenuController, private nav: NavController, private authService: AuthServiceProvider, public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private formBuilder: FormBuilder, private validationService: ValidationServiceProvider) {
    //this.loggedIn = this.userLoggedIn(); 
    this.setLastUsedEmail()
    this.buildLoginForm()
    //this.storage.clear()
  }
  fblogin() {
    //this.browser.create('http://facebook.com')
    /*
    try {
      console.log('Yay..!')
      this.fb.login().then((data) => {
        console.log('data -> ', data)
      })
    }
    catch (e) {
      console.error(e)
    }
    */
  }

  buildLoginForm() {
    let validationService = this.validationService
    this.logInForm = this.formBuilder.group(
      {
        'email': ['', [Validators.required, validationService.emailValidator]],
        'password': ['', [Validators.required, Validators.minLength(6)]]
      }
    )
  }

  setLastUsedEmail() {
    this.storage.get('lastUsedEmail').then(email => {
      this.LastUsedEmail = email
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  logIn() {
    //  this.authService.logInAndAssignTokenAndData(loginParams);
    if (this.logInForm.dirty && this.logInForm.valid) {
      this.http.post(`${this.url}sign_in`, this.logInForm.value).subscribe(response => {
        console.log(response)
        this.authToken = response.headers.get('access-token');
        this.userData = response.json()['data'];
        this.dataService.saveUserData(this.userData);
        this.dataService.saveToken(this.authToken);
        this.dataService.persistStatusDataLocally(this.userData['id'])
        this.authenticateAndPush();
      },
        error => {
          this.errors = error.json().errors
        })
    }
  }

  authenticateAndPush() {
    // if (this.getToken() != null) {
    this.nav.push(ItemsPage);
    this.menuController.enable(false, 'unauthenticated');
    this.menuController.enable(true, 'authenticated');
    // }
  }

  getUserData() {
    let data: any;
    this.storage.get('userData').then((val) => {
      data = JSON.parse(val);
    });
    return data;
  }

  signUp() {
    this.navCtrl.push(SignupPage);
  }
}