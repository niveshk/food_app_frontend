import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage'

@Injectable()
export class DataServiceProvider {
  url: string = 'http://food-app-thenightsaredarkandfullofterrors.c9users.io:8080';
  user_id: number;
  cart_items: Array<any> = [];
  shopping_items: Array<any> = [];
  constructor(public http: Http, private storage: Storage) {

  }

  
  getToken() {
    return this.storage.get('access-token');
  }

  getData() {
    return this.storage.get('userData');
  }
  saveUserData(data) {
    this.storage.set('userData', JSON.stringify(data));
  }
  saveToken(token) {
    this.storage.set('access-token', token);
  }

  saveInstructionStatuses(instStatuses, recipe_id) {
    this.storage.set(`${recipe_id}_instruction_statuses`, JSON.stringify(instStatuses));
  }

  clearStorageOfAuthDetails() {
    this.storage.remove('userData')
    this.storage.remove('access-token')
  }

  getInstructionStatuses(recipe_id) {
    let keyName = `${recipe_id}_instruction_statuses`;
    return this.storage.get(keyName);
  }

  saveIngStatuses(ingStatuses, recipe_id) {
    // console.log(ingStatuses)
    this.storage.set(`${recipe_id}_ingredient_statuses`, JSON.stringify(ingStatuses));
  }

  getIngredientStatuses(recipe_id) {
    let keyName = `${recipe_id}_ingredient_statuses`;
    return this.storage.get(keyName);
  }

  postStatusData(user_id) {
    this.storage.forEach((value, key) => {
      if (key.match(/\d_instruction_statuses/)) {

        let digit = key.charAt(0);
        let instruction_statuses = JSON.parse(value);

        this.http.post(`${this.url}/users/${user_id}/recipes/${digit}`, { "recipe_status": { "instruction_statuses": instruction_statuses } })
          .subscribe((response) => {
            //console.log(response);
          });
      }
      else if (key.match(/\d_ingredient_statuses/)) {
        let digit = key.charAt(0);
        let ingredient_statuses = JSON.parse(value);
        this.http.post(`${this.url}/users/${user_id}/recipes/${digit}`, { "recipe_status": { "ingredient_statuses": ingredient_statuses } })
          .subscribe((response) => {
            //  console.log(response);
          });
      }
      else if (key.match(/\d_shopping_bag_status/)) {
        let digit = key.charAt(0)
        let shopping_bag_status = JSON.parse(value)
        this.http.post(`${this.url}/users/${user_id}/recipes/${digit}`, { "recipe_status": { "shopping_bag_status": shopping_bag_status } })
          .subscribe(response => {
            //console.log(response)
          });
      }
    });
  }

  getUserId() {
    this.getData();
  }
  getIngredientStatusesFromApi(recipe_id, user_id) {
    return this.http.get(`${this.url}/users/${user_id}/recipes/${recipe_id}/ingredient_statuses`);
  }
  getRecipeStatusFromApi(user_id, recipe_id) {
    return this.http.get(`${this.url}/users/${user_id}/recipes/${recipe_id}`);

  }

  getInstructionStatusesFromApi(recipe_id, user_id) {
    return this.http.get(`${this.url}/users/${user_id}/recipes/${recipe_id}/instruction_statuses`);
  }

  postReview(recipe_id, content, rating) {
    let review = {
      content: content,
      rating: rating
    }
    return new Promise(resolve => {
      this.storage.get('userData').then(data => {
        this.user_id = JSON.parse(data).id;
        this.http.post(`${this.url}/users/${this.user_id}/recipes/${recipe_id}/reviews`, review).subscribe(res => {
          resolve(res);
        }
        );
      }).catch((err) => {
        console.log(err)
      })


    })
  }
  getReviews(recipe) {
    return this.http.get(`${this.url}/items/${recipe.item_id}/recipes/${recipe.id}/reviews`);
  }
  addToCart(item, user_id) {
    this.cart_items.push(item)

    return this.http.post(`${this.url}/users/${user_id}/cart_items/${item.id}`, item.imageUrls[0])

  }
  getCartItems(user_id) {
    return this.http.get(`${this.url}/users/${user_id}/cart_items`).map(data => data.text());

  }
  getNutrition(item_id, recipe_id) {
    return this.http.get(`${this.url}/items/${item_id}/recipes/${recipe_id}/nutrition`);
  }
  saveShoppingBag(shoppingBagStatus, recipe_id) {
    this.storage.set(`${recipe_id}_shopping_bag_status`, JSON.stringify(shoppingBagStatus))
  }
  removeFromShoppingBag(shoppingBagStatuses, recipe_id, ing, user_id) {
    this.storage.set(`${recipe_id}_shopping_bag_status`, JSON.stringify(shoppingBagStatuses))
  }

  getShoppingBagStatuses(recipe_id) {
    return this.storage.get(`${recipe_id}_shopping_bag_status`)
  }
  addToPocket(recipe_id, user_id) {
    //this.http.post(`{this.url}/ `)
    this.storage.set(`user_${user_id}_recipe_${recipe_id}_added`, true)
    return this.http.post(`${this.url}/users/${user_id}/recipes/${recipe_id}/pocket`, {}).map(data => data.json())
  }
  checkLocallyIfAdded(recipe_id,user_id) {
    return this.storage.get(`user_${user_id}_recipe_${recipe_id}_added`)
  }
  checkRemotelyIfAdded(recipe_id, user_id) {}
  removeFromPocket(recipe_id, user_id) {
    this.storage.set(`user_${user_id}_recipe_${recipe_id}_added`, false)
    return this.http.delete(`${this.url}/users/${user_id}/recipes/${recipe_id}/pocket`).map(data => data.json())    
  }
  updateLocalPocket(pocket) {
    this.storage.set("pocket", JSON.stringify(pocket))
  }

  getPocketFromStorage() {
    return this.storage.get("pocket")
  }
  getShoppingBag(user_id) {
    return new Promise((resolve, reject) => {
      let statuses: Array<Number> = []
      let actualStatuses = {}
      let ingredients = []
      let selectedIngredients = []
      this.storage.forEach((value, key) => {
        if (key.match(/\d_shopping_bag_status/)) {
          value = JSON.parse(value)
          let keyForHash = key.charAt(0)
          actualStatuses[keyForHash] = value
          for (let key1 in value) {
            if (value[key1]) {
              statuses.push(parseInt(key1))
            }
          }
        }
        else if (key.match(/\d_ingredients/)) {
          value = JSON.parse(value)
          ingredients.push(...value)
        }
      }).then(() => {
        selectedIngredients = ingredients.filter(ele => {
          return statuses.includes(ele["id"])
        })
        resolve([selectedIngredients, actualStatuses, statuses])
      })
    })
  }
  findAndReturnIngredientById(id, ingredients) {
    console.log(ingredients)
  }
  removeFromBag(item) {

  }

  getIngredientAndShoppingBagStatuses(recipe_id) {
    let status = {}
    return new Promise(resolve => {
      this.storage.get(`${recipe_id}_ingredient_statuses`).then(ingredient_statuses => {
        this.storage.get(`${recipe_id}_shopping_bag_status`).then(shopping_bag_status => {
          status["ingredient_statuses"] = JSON.parse(ingredient_statuses)
          status["shopping_bag_status"] = JSON.parse(shopping_bag_status)
          resolve(status)
        })
      })
    })
  }
  persistStatusDataLocally(userId) {
    this.http.get(`${this.url}/users/${userId}/recipe_statuses/`).map(data => data.json()).subscribe(recipeStatuses => {
      recipeStatuses.forEach((recipeStatus) => {
        this.storage.set(`${recipeStatus.recipe_id}_ingredient_statuses`, JSON.stringify(recipeStatus.ingredient_statuses))
        this.storage.set(`${recipeStatus.recipe_id}_instruction_statuses`, JSON.stringify(recipeStatus.instruction_statuses))
        this.storage.set(`${recipeStatus.recipe_id}_shopping_bag_status`, JSON.stringify(recipeStatus.shopping_bag_status))
      })
    })
  }

  public persistIngredientsByRecipeId(ingredients, recipe_id) {
    this.storage.set(`${recipe_id}_ingredients`, JSON.stringify(ingredients))
  }
  public getIngredientsFromStorage(recipe_id) {
    return this.storage.get(`${recipe_id}_ingredients`)
  }

  saveStrikes(strikes) {
    this.storage.set('strikes', JSON.stringify(strikes))
  }
  getStrikes(){
    return this.storage.get('strikes')
  }
}


