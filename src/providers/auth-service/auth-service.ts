import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';
@Injectable()
export class AuthServiceProvider {
  loggedIn : boolean;
  authToken : any;
  url : string = 'http://food-app-thenightsaredarkandfullofterrors.c9users.io/auth/';
  userData : any;
  constructor(private storage : Storage, public http : Http) {
    
  }
  logInAndAssignTokenAndData(loginParams){
    this.http.post(`${this.url}sign_in`, loginParams).subscribe((response)=>{
      this.authToken = response.headers.get('access-token');
      this.userData = response.json();
      this.storage.set('userData', JSON.stringify(this.userData));
      this.storage.set('access-token', this.authToken);
  });
  }
  getToken(){
    this.storage.get('access-token').then((val) => {
      this.authToken = val;
      
    });
    return this.authToken;
  }
  
  getData(){
    return this.storage.get('userData');
  }

  signUp(signUpParams){
    console.log(signUpParams);
    return this.http.post(`${this.url}`, signUpParams);
  }
  saveUserData(data){
    this.storage.set('userData', JSON.stringify(data));
  }
  saveToken(token){
    this.storage.set('access-token', token);
  }
}


