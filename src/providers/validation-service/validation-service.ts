import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class ValidationServiceProvider {
  
  constructor(public http: Http) {

  }

  emailValidator(email) {
    let EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (email.value.match(EMAIL_REGEX)) {
      return null
    }
    else {
      return { 'Invalid Email Address': true }
    }
  }
  passwordValidator(passwordControl) {
    if (passwordControl.value.length < 6) {
      return {"Password should be larger than 6 characters" : true}
    }
    return null;
  }

}
